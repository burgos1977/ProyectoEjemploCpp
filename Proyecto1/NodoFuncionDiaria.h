#ifndef NodoFuncionDiaria_H
#define NodoFuncionDiaria_H

#pragma once
#include "ListaPelicula.h"
#include "Horario.h"
#include "Sala.h"

using namespace std;
class NodoFuncionDiaria
{
private:
	Pelicula* aPelicula;
	Horario aHorario;
	Sala aSala;
	NodoFuncionDiaria* aSiguiente;

public:
	NodoFuncionDiaria(string, string, string, Pelicula*);
	~NodoFuncionDiaria(void);
	void setPelicula(Pelicula*);
	Pelicula* getPelicula();
	void setHorario(Horario);
	Horario getHorario();
	void setSala(Sala);
	Sala* getSala();
	void setSiguiente(NodoFuncionDiaria*);
	NodoFuncionDiaria* getSiguiente();
};
#endif

