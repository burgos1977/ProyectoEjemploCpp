#ifndef OrdenComra_H
#define OrdenCompra_H

#pragma once
#include <sstream>
#include <string>

using namespace std;
class OrdenCompra
{
private:
	string aCodigoPelicula;
	string aDiaFuncion;
	string aHorarioFuncion;
	string aSalaFuncion;
	string aTipoSalaFuncion;
	int aCantidadAdultos;
	int aCantidadNinnos;

public:
	OrdenCompra();
	~OrdenCompra(void);
	void setCodigoPelicula(string);
	string getCodigoPelicula();
	void setDiaFuncion(string);
	string getDiaFuncion();
	void setHorarioFuncion(string);
	string getHorarioFuncion();
	void setSalaFuncion(string);
	string getSalaFuncion();
	void setTipoSalaFuncion(string);
	string getTipoSalaFuncion();
	void setCantidadAdultos(int);
	int getCantidadAdultos();
	void setCantidadNinnos(int);
	int getCantidadNinnos();
	double MontoPagar(int, int);
	string Imprimir();
};
#endif
