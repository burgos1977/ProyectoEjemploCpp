#ifndef Horario_H
#define Horario_H

#pragma once
#include <sstream>
#include <string>

using namespace std;
class Horario
{
private:
	string aHorario;

public:
	Horario();
	Horario(string);
	~Horario(void);
	void setHorario(string);
	string getHorario();
	string Imprimir();
};
#endif
