#include "Horario.h"

//Constructor sin parametros de la clase
Horario::Horario()
{
}

//Constructor con parametros de la clase
Horario::Horario(string pHorario)
{
	aHorario = pHorario;
}

//Metodo que se encarga de modificar el valor de la hora
void Horario::setHorario(string pHorario)
{
	aHorario = pHorario;
}

//Metodo que se encarga de retornar el valor de la hora
string Horario::getHorario()
{
	return aHorario;
}

//Destructor de la clase
Horario::~Horario(void)
{
}

//Metodo encargado de imprimir en pantalla la hora en formato hora:minutos
string Horario::Imprimir()
{
	stringstream lvMensaje;
	lvMensaje << "Horario "<< getHorario();
	return lvMensaje.str();
}