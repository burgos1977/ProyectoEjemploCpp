#include "Asiento.h"

//Constructor sin parametros de la clase, inicializa el estado en D.
Asiento::Asiento()
{
	aEstado = 'D';
}

// Metodo encargado de actualizar el estado del asiento, es de tipo bool debido a una medida de seguridad implementada con el fin de que
//el estado cambie solo bajo ciertas condiciones como por ejemplo el echo de que no el asiento no puede cambiar de disponible a ocupado
//o de forma contraria.
void Asiento::setEstado(char pEstado)
{

	aEstado = pEstado;
}

//Metodo que se encarga de retornar el valor del estado ya sea D(disponible), S(seleccionado) u O(ocupado)
char Asiento::getEstado()
{
	return aEstado;
}

//Destructor de la clase 
Asiento::~Asiento(void)
{
}

//Inicializa el asiento en D, la intencion de este metodo es iniciar de nuevo el estado de dicho asiento sin tener que actualizarlo
void Asiento::Inicializar()
{
	aEstado = EstadoDisponible();
}

//Metodo que se encarga de retornar el estado disponible mediante un char, la intencio es facilitar el uso de dicho char en diferentes 
//metodos ya que simplemente si se desea otro caracter diferente al mostrado, se cambia en este metodo y por ende en cualquier otro 
//metodo que sea usado cambia automaticamente
char Asiento::EstadoDisponible()
{
	return 'D';
}

//Metodo que se encarga de retornar el estado ocupado mediante un char, la intencio es facilitar el uso de dicho char en diferentes 
//metodos ya que simplemente si se desea otro caracter diferente al mostrado, se cambia en este metodo y por ende en cualquier otro 
//metodo que sea usado cambia automaticamente
char Asiento::EstadoOcupado()
{
	return 'O';
}

//Metodo que se encarga de retornar el estado seleccionado mediante un char, la intencio es facilitar el uso de dicho char en diferentes 
//metodos ya que simplemente si se desea otro caracter diferente al mostrado, se cambia en este metodo y por ende en cualquier otro 
//metodo que sea usado cambia automaticamente
char Asiento::EstadoSeleccionado()
{
	return 'S';
}

