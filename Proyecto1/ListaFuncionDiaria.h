#ifndef ListaFuncion_H
#define ListaFuncion_H

#pragma once
#include "NodoFuncionDiaria.h"

using namespace std;
class ListaFuncionDiaria
{
public:
	int aContador;
	string aNombre;
	NodoFuncionDiaria* aNodoFuncionDiaria;

public:
	ListaFuncionDiaria();
	~ListaFuncionDiaria(void);
	void setNombre(string);
	string getNombre();
	bool ListaVacia();
	bool ExisteFuncion(string, string, string);
	bool CompararFuncion(string, string, string, NodoFuncionDiaria*);
	bool AgregarFinal(string, string, string, Pelicula*);
	bool Elimina(Pelicula*, string, string, string);
	string Imprimir();
	string ImpresionCompleta();
	string ImprimirXCodigo(string);
	Sala* BuscarSala(string, string, string);
};
#endif

