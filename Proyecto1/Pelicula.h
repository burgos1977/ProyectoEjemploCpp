#ifndef Pelicula_H
#define Pelicula_H

#pragma once
#include <sstream>
#include <string>

using namespace std;
class Pelicula
{
private:
	string aCodigo;
	string aNombre;
	string aGenero;
	string aTipo;
	string aTipoPublico;
	string aIdioma;
	string aSinopsis;

public:
	Pelicula(string, string, string, string, string, string, string);
	~Pelicula(void);
	void setCodigo(string);
	string getCodigo();
	void setNombre(string);
	string getNombre();
	void setGenero(string);
	string getGenero();
	void setTipo(string);
	string getTipo();
	void setTipoPublico(string);
	string getTipoPublico();
	void setIdioma(string);
	string getIdioma();
	void setSinopsis(string);
	string getSinopsis();
	string Imprimir(string);
};
#endif
