#include "OrdenCompra.h"

//Metodo constructor sin parametros
OrdenCompra::OrdenCompra()
{
	aCodigoPelicula = "";
	aDiaFuncion = "";
	aCantidadAdultos = 0;
	aCantidadNinnos = 0;
}

//Metodos set y get de la clase------------------------------------------
void OrdenCompra::setCodigoPelicula(string pCodigoPelicula)
{
	aCodigoPelicula = pCodigoPelicula;
}

string OrdenCompra::getCodigoPelicula()
{
	return aCodigoPelicula;
}

void OrdenCompra::setDiaFuncion(string pDiaFuncion)
{
	aDiaFuncion = pDiaFuncion;
}

string OrdenCompra::getDiaFuncion()
{
	return aDiaFuncion;
}

void OrdenCompra::setHorarioFuncion(string pHorarioFuncion)
{
	aHorarioFuncion = pHorarioFuncion;
}

string OrdenCompra::getHorarioFuncion()
{
	return aHorarioFuncion;
}

void OrdenCompra::setSalaFuncion(string pSalaFuncion)
{
	aSalaFuncion = pSalaFuncion;
}

string OrdenCompra::getSalaFuncion()
{
	return aSalaFuncion;
}

void OrdenCompra::setTipoSalaFuncion(string pTipoSalaFuncion)
{
	aTipoSalaFuncion = pTipoSalaFuncion;
}

string OrdenCompra::getTipoSalaFuncion()
{
	return aTipoSalaFuncion;
}

void OrdenCompra::setCantidadAdultos(int pCantidadAdultos)
{
	aCantidadAdultos = pCantidadAdultos;
}

int OrdenCompra::getCantidadAdultos()
{
	return aCantidadAdultos;
}

void OrdenCompra::setCantidadNinnos(int pCantidadNinnos)
{
	aCantidadNinnos = pCantidadNinnos;
}

int OrdenCompra::getCantidadNinnos()
{
	return aCantidadNinnos;
}
//------------------------------------------------------------------------------

//Metodo destructor de la clase
OrdenCompra::~OrdenCompra(void)
{
}

//Metodo encargado de determinar el total a pagar segun el tipo de sala y el tipo de persona
double OrdenCompra::MontoPagar(int pCantidadNinnos, int pCantidadAdultos)
{
	double lvPecioAdulto = 0;
	double lvPecioNinno = 0;
	double lvResultado = 0;
	if (getTipoSalaFuncion() == "Regular")
	{
		lvPecioNinno = pCantidadNinnos * 3000;
		lvPecioAdulto = pCantidadAdultos * 5000;
		lvResultado = lvPecioAdulto + lvPecioNinno;
	}
	else
		if (getTipoSalaFuncion() == "VIP")
		{
			lvPecioNinno = pCantidadNinnos * 4500;
			lvPecioAdulto = pCantidadAdultos * 6500;
			lvResultado = lvPecioAdulto + lvPecioNinno;
		}
		return lvResultado;
}

//Imprime todos los datos de la orden de compra
string OrdenCompra::Imprimir()
{
	stringstream lvMensaje;
	lvMensaje << "Pelicula: " << getCodigoPelicula() << "\n";
	lvMensaje << "Dia: " << getDiaFuncion() << "\n";
	lvMensaje << "Sala: " << getSalaFuncion() << " " << getTipoSalaFuncion() << "\n";
	lvMensaje << "Horario: " << getHorarioFuncion() << "\n";
	lvMensaje << "Cantidad ticketes ninno: " << getCantidadNinnos() << "\n";
	lvMensaje << "Cantidad ticketes adulto: " << getCantidadAdultos() << "\n";
	lvMensaje << "Monto total a pagar: $ " << MontoPagar(aCantidadNinnos, aCantidadAdultos);
	return lvMensaje.str();
}
