#ifndef Asientos_H
#define Asientos_H

#pragma once
#include "Asiento.h"
#include <sstream>
#include <string>
#include <windows.h>
#include <iostream>


using namespace std;
const char caMAX_FILAS = 6;
const char caMAX_COLUMNAS = 10;
class Asientos
{
private:
	int aDisponibilidadAsientos;
	Asiento aAsientos[caMAX_FILAS][caMAX_COLUMNAS];
	//Atributos que se encargan de la impresion de los asientos con color
	int aFilaActualImpresion;
	int aColumnaActualImpresion;
	bool aImpresionFinalizada;
	////---
public:
	Asientos();
	~Asientos(void);
	void setEstadoAsiento(int, int, char);
	char getEstadoAsiento(int, int);
	int getDisponibilidadAsientos();
	bool SeleccionarAsiento(char, int);
	void OcuparAsiento(/*int, int*/);
	void InicializarAsientos();
	string Imprimir();
	void setcolor(int color);
	char ObtenerLetraFila(int pIndice);
	void InicializarImpresionAsientos();
	bool AvanzarImpresion();
	bool ImpresionFinalizada();
	char getFilaActualImpresion();
	char getEstadoAsientoActualImpresion();
	void ImprimirAsiento(char);
	////----
};
#endif;
