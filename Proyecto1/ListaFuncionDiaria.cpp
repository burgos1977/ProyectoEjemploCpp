#include "ListaFuncionDiaria.h"

//Constructor con parametros de la clase
ListaFuncionDiaria::ListaFuncionDiaria()
{
	aNombre = "";
	aContador = 0;
	aNodoFuncionDiaria = NULL;
}

//Metodo que actualiza el valor del nombre
void ListaFuncionDiaria::setNombre(string pNombre)
{
	aNombre = pNombre;
}

//Metodo encargado de retornar el valor del nombre
string ListaFuncionDiaria::getNombre()
{
	return aNombre;
}

//Destructor de la clase
ListaFuncionDiaria::~ListaFuncionDiaria(void)
{
	if (ListaVacia() == false)
	{
		NodoFuncionDiaria* lvAnterior = aNodoFuncionDiaria;
		NodoFuncionDiaria* lvActual = aNodoFuncionDiaria -> getSiguiente();
		while (lvActual != NULL)
		{
			lvAnterior -> setSiguiente(lvActual -> getSiguiente());/*Anterior pasa a apuntar al valor del siguiente de actual*/
			delete (lvActual);
			lvActual = NULL;
			lvAnterior = lvActual;
			lvActual = lvAnterior -> getSiguiente();
		}
	}
}

//Metodo que se encarga de verificar que la lista contenga o no informacion
bool ListaFuncionDiaria::ListaVacia()
{
	bool lvValorRetorno = false;
	if (aContador == 0)
		lvValorRetorno = true;
	return lvValorRetorno;
}

//Metodo encargado de buscar las funciones por sus valores y determinar si existe
bool ListaFuncionDiaria::ExisteFuncion(string pNombreSala, string pTipoSala, string pHorario)
{
	bool lvValorRetorno = false;
	NodoFuncionDiaria* lvActual = aNodoFuncionDiaria;
	Sala* lvSala;
	Horario lvHorario;
	if (ListaVacia() == false)
	{
		while ((lvActual != NULL)&&(lvValorRetorno == false))
		{
			lvHorario = lvActual -> getHorario();
			lvSala = lvActual -> getSala();
			if ((pNombreSala == lvSala->getNombre())&&(pTipoSala == lvSala->getTipo())&&(pHorario == lvHorario.getHorario()))
				lvValorRetorno = true;
			else
				lvActual = lvActual -> getSiguiente();
		}
	}
	return lvValorRetorno;
}

//Metodo encargado de verificar si una funcion se encuentra en la lista o no de acuerdo a los valores que contiene
bool ListaFuncionDiaria::CompararFuncion(string pNombreSala, string pTipoSala, string pHorario, NodoFuncionDiaria* pNodoFuncionDiaria)
{
	bool lvValorRetorno = false;
	Sala* lvSala;
	Horario lvHorario;
	if (pNodoFuncionDiaria != NULL)
	{
		lvHorario = pNodoFuncionDiaria -> getHorario();
		lvSala = pNodoFuncionDiaria -> getSala();
		if ((pNombreSala == lvSala->getNombre())&&(pTipoSala == lvSala->getTipo())&&(pHorario == lvHorario.getHorario()))
			lvValorRetorno = true;
	}
	return lvValorRetorno;
}

//Metodo encargado de agregar una pelicula a la lista, se realiza el metodo de manera que al agregar una pelicula se agregue automaticamente al inicio 
bool ListaFuncionDiaria::AgregarFinal(string pHorario, string pTipoSala, string pNombreSala, Pelicula* pPelicula)
{
	bool lvValorRetorno = false;
	//Se utiliza un condicional para contemplar los dos panoramas si existe alguna pelicula o no existe ninguna
	if (ListaVacia() == true)
	{
		aNodoFuncionDiaria = new NodoFuncionDiaria(pHorario, pTipoSala, pNombreSala, pPelicula);
		lvValorRetorno = true;
		aContador++;
	}
	else
	{
		//Ya existen peliculas
		if (ExisteFuncion(pNombreSala, pTipoSala, pHorario) == false)/*Se verifica si la pelicula ya esta porque si ya esta no se puede ingresar otra vez.*/
		{
			NodoFuncionDiaria* lvAuxiliar;
			NodoFuncionDiaria* lvActual = aNodoFuncionDiaria;
			//Ciclo que hace que el actual se situe en la ultima posicion
			while (lvActual ->getSiguiente() != NULL)
			{
				lvActual = lvActual -> getSiguiente();
			}
			//Creo una variable a la cual le almaceno la informacion del nuevo nodo
			lvAuxiliar = new NodoFuncionDiaria(pHorario, pTipoSala, pNombreSala, pPelicula);
			lvActual -> setSiguiente(lvAuxiliar);
			lvValorRetorno = true;
			aContador++;
		}
	}
	return lvValorRetorno;
}

//Metodo que se encarga de eliminar una pelicula determinada por medio del codigo de pelicula
bool ListaFuncionDiaria::Elimina(Pelicula* pPelicula, string pNombreSala, string pTipoSala, string pHorario)
{
	bool lvValorRetorno = false;
	if(ListaVacia() == false)
	{
		if(aContador == 1)
		{
			//En este caso solo existe una pelicula
			if (CompararFuncion(pNombreSala, pTipoSala, pHorario, aNodoFuncionDiaria) == true)
			{
				delete (aNodoFuncionDiaria);
				aNodoFuncionDiaria = NULL;
			}
		}
		else
		{
			//Existen mas de una pelicula
			NodoFuncionDiaria* lvAnterior = aNodoFuncionDiaria;
			NodoFuncionDiaria* lvActual = aNodoFuncionDiaria -> getSiguiente();
			while (lvActual != NULL)
			{
				if (CompararFuncion(pNombreSala, pTipoSala, pHorario, lvActual) == true)/*invoca al metodo comparar para determinar si existe la funcion en el nodo actual*/
				{
					lvAnterior -> setSiguiente(lvActual -> getSiguiente());/*Anterior pasa a apuntar al valor del siguiente de actual*/
					delete (lvActual);
					lvActual = NULL;
					lvValorRetorno = true;/*se logro eliminar*/
				}
				else
				{
					//actual y anterior avanzan para seguir buscando el codigo a eliminar
					lvAnterior = lvActual;
					lvActual = lvAnterior -> getSiguiente();
				}
			}
		}
	}
	return lvValorRetorno;
}

//Metodo encargado de imprimir en pantalla la lista
string ListaFuncionDiaria::Imprimir()
{
	stringstream lvMensaje;
	NodoFuncionDiaria* lvActual = aNodoFuncionDiaria;
	Pelicula* lvPelicula = NULL;
	Horario lvHorario;
	Sala* lvSala;
	string lvHorarioRetorno = " ";
	int lvContador = 1;

	while(lvActual != NULL)
	{
		lvPelicula = lvActual->getPelicula();
		lvHorario = lvActual -> getHorario(); 
		lvSala = lvActual -> getSala();
		lvHorarioRetorno = lvHorario.Imprimir();
		lvMensaje << "|" << lvContador << ") " << lvPelicula -> getCodigo() << "/" << lvHorarioRetorno << "/" << lvSala->getNombre() << "|" << "\n";
		lvContador++;
		lvActual = lvActual -> getSiguiente();
	}
	return lvMensaje.str();
}

//Metodo que imprime toda la informacion de toda la lista
string ListaFuncionDiaria::ImpresionCompleta()
{
	stringstream lvMensaje;
	NodoFuncionDiaria* lvActual = aNodoFuncionDiaria;
	Pelicula* lvPelicula = NULL;
	Horario lvHorario;
	Sala* lvSala;
	string lvHorarioRetorno = " ";
	int lvContador = 1;

	while(lvActual != NULL)
	{
		lvPelicula = lvActual->getPelicula();
		lvHorario = lvActual -> getHorario(); 
		lvSala = lvActual -> getSala();
		lvMensaje << getNombre() << "\n";
		lvMensaje << lvSala->ImprimirSala() << "\n";
		lvMensaje << lvHorario.Imprimir() << "\n";
		lvMensaje << lvPelicula -> Imprimir(lvPelicula -> getCodigo()) << "\n"; 
		lvMensaje << "\n--------------------------------------------------/*Siguiente pelicula*\-------------------------------------------------\n";
		lvActual = lvActual -> getSiguiente();
	}
	return lvMensaje.str();
}

//Metodo encargado de imprimir en pantalla la lista
string ListaFuncionDiaria::ImprimirXCodigo(string pCodigoPelicula)
{
	stringstream lvMensaje;
	NodoFuncionDiaria* lvActual = aNodoFuncionDiaria;
	Pelicula* lvPelicula = NULL;
	Horario lvHorario;
	Sala* lvSala;
	string lvHorarioRetorno = " ";
	int lvContador = 1;

	while(lvActual != NULL)
	{
		lvPelicula = lvActual->getPelicula();
		lvHorario = lvActual -> getHorario(); 
		lvSala = lvActual -> getSala();
		lvHorarioRetorno = lvHorario.Imprimir();
		if (lvPelicula -> getCodigo() == pCodigoPelicula)
		{
			lvMensaje << "|" << lvContador << ") " << lvPelicula -> getCodigo() << "/" << lvHorarioRetorno << "/" << lvSala->getNombre() << "(" << lvSala->getTipo() << ")"<<"|" << "\n";
			lvContador++;
		}
		lvActual = lvActual -> getSiguiente();
	}
	return lvMensaje.str();
}

//Metodo que busca una sala especifica, se determina a partir del nombre de la funcion, el horario de la funcion y el codigo de la pelicula
Sala* ListaFuncionDiaria::BuscarSala(string pNombreFuncion, string pHorario, string pCodigoPelicula)
{
	NodoFuncionDiaria* lvActual = aNodoFuncionDiaria;
	Pelicula* lvPelicula = NULL;
	Horario lvHorario;
	bool lvValorEncontrado = false;
	Sala* lvSalaRetorno = NULL;
	while((lvActual != NULL)&&(lvValorEncontrado == false))
	{
		lvPelicula = lvActual -> getPelicula();
		lvHorario = lvActual -> getHorario();
		if ((lvPelicula -> getCodigo() == pCodigoPelicula)&&(getNombre() == pNombreFuncion)&&(lvHorario.getHorario() == pHorario))
		{
			lvSalaRetorno = lvActual -> getSala();
			lvValorEncontrado = true;
		}
		else
			lvActual=lvActual ->getSiguiente();
	}
	return lvSalaRetorno;
}