#ifndef ListaPelicula_H
#define ListaPelicula_H

#pragma once
#include <sstream>
#include <string>

#include "NodoPelicula.h"

using namespace std; 
class ListaPelicula
{
private:
	int aContador;

	NodoPelicula* aNodoPelicula;

public:
	ListaPelicula();
	~ListaPelicula(void);
	bool ListaVacia();
	bool ExistePelicula(string);
	bool AgregarInicio(string, string, string, string, string, string, string);
	bool CompararPelicula(string, NodoPelicula*);
	bool Elimina(string pCodigo);
	string Imprimir();
	string ImprimirXCodigo(string);
	Pelicula* BuscarPelicula(string);
	string BusquedaNombre(string);
	string BusquedaGenero(string);
	string BusquedaTipo(string);
	string BusquedaTipoPublico(string);
	string BusquedaIdioma(string);
};
#endif