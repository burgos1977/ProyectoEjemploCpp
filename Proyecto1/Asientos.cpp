#include "Asientos.h"
#include <conio.h>
#include <string.h>

//Constructor sin parametros de la clase
Asientos::Asientos()
{
	aDisponibilidadAsientos = caMAX_FILAS;
	//cliclos que se encargan de recorrer la matriz de asientos y actualizar el estado disponible
	for (int i = 0; i< caMAX_FILAS; i++)
	{
		for (int j = 0; j < caMAX_COLUMNAS; j++)
		{
			aAsientos[i][j].setEstado(aAsientos[i][j].EstadoDisponible());
		}
	}
	aFilaActualImpresion = 0;
	aColumnaActualImpresion = 0;
	aImpresionFinalizada = false;
}

//Metodo que se encarga de modificar el valor de una posicion especifica de la matriz, tomando en cuenta como seguridad
//que el valor ingresado solo puede ser D(disponible), S(seleccionado) u O(ocupado), ademas posee otra funcionalidad
//la cual consiste en codificar el atributo encargado de contar la cantidad de asientos que estan disponibles(D) en la matriz de asientos
void Asientos::setEstadoAsiento(int pFila, int pColumna, char pValor)
{
	//Condicional que controla que el valor que se va a ingresar al  estado solo puede ser S(seleccionado) u O(ocupado)
	if ((pValor == 'O') || (pValor == 'S'))
	{
		aAsientos[pFila][pColumna].setEstado(pValor);
		//La idea de disminuir los asientos es que una vez seleccionado u ocupado un asiento se reduzca la cantidad de 
		//asientos disponibles
		aDisponibilidadAsientos--;
	}
	//Si el valor ingresado no es S(seleccionado) u O(ocupado) debe ser D(disponible) ya que el metodo setEstado
	//solo aceptara una D, S u O debido a la restriccion que limita el valor ingresado al estado.  
	else
	{
		//La idea de aumentar los asientos es que una vez actualizado el estado de un asiento en disponible
		//se incremente la cantidad de asientos disponibles
		aAsientos[pFila][pColumna].setEstado(pValor);
		aDisponibilidadAsientos++;
	}
}

//Metodo que se encarga de retornar el valor de una posicion en especifico mediante el ingreso de una fila y columna en especifico
char Asientos::getEstadoAsiento(int pFila, int pColumna)
{
	return aAsientos[pFila][pColumna].getEstado();
}

//Metodo que se encarga de retornar la cantidad de asientos disponibles
int Asientos::getDisponibilidadAsientos()
{
	return aDisponibilidadAsientos;
}

//Destructor de la clase
Asientos::~Asientos(void)
{
}

//Toda las posiciones de la matriz se inicializan en D, quiere decir que todos los asientos se encuentran disponibles
void Asientos::InicializarAsientos()
{
	for (int lvFila = 0; lvFila < caMAX_FILAS; lvFila++)
		for (int lvColumna = 0; lvColumna < caMAX_COLUMNAS; lvColumna++)
			aAsientos[lvFila][lvColumna].Inicializar();
}

//Metodo que se encarga de retornar una letra que se utiliza al inicio de cada fila de la matriz como identificador de la fila
char Asientos::ObtenerLetraFila(int pIndice)
{
	char lvRetorno = ' ';
		if ( pIndice == 0)
			lvRetorno = 'A';
		else
			if ( pIndice == 1)
				lvRetorno = 'B';
			else
				if ( pIndice == 2)
					lvRetorno = 'C';
				else
					if ( pIndice == 3)
						lvRetorno = 'D';
					else
						if ( pIndice == 4)
							lvRetorno = 'E';
						else
							if ( pIndice == 5)
								lvRetorno = 'F';
		return lvRetorno;
}

//Una vez seleccionado un asiento, si se confirma, este metodo lo establece en ocupado
void Asientos::OcuparAsiento()
{
	for (int i = 0; i < caMAX_FILAS; i++)
	{
		for (int j = 0; j < caMAX_COLUMNAS; j++)
		{
			if (aAsientos[i][j].getEstado() == 'S')
			{
				aAsientos[i][j].setEstado('O');
			}
		}
	}
}

//Metodo encargado de modificar el valor de un asiento a O(ocupado), este metodo tiene como proposito que el usuario se guie
//en las filas de la matriz por medio de las letras que utiliza al inicio de cada una de estas, y que a la hora de ingresar la letra
//sin importar el CAPSLOCK pueda imgresar la fila y que pueda actualizar el estado de un asiento especifico
bool Asientos::SeleccionarAsiento(char pFila, int pColumna)
{
	bool lvRetorno = false;
	if (((pFila == 'A')||(pFila == 'a')) || (pFila == '1'))
	{
		if(getEstadoAsiento(0, pColumna)=='D')
		{
			setEstadoAsiento(0, pColumna, 'S');
			lvRetorno = true;
		}
	}
	else
		if (((pFila == 'B')||(pFila == 'b')) || (pFila == '2'))
		{
			if(getEstadoAsiento(1, pColumna)=='D')
			{
				setEstadoAsiento(1, pColumna, 'S');
				lvRetorno = true;
			}
		}
		else
			if (((pFila == 'C')||(pFila == 'c')) || (pFila == '3'))
			{
				if(getEstadoAsiento(2, pColumna)=='D')
				{
					setEstadoAsiento(2, pColumna, 'S');
					lvRetorno = true;
				}
			}
			else
				if (((pFila == 'D')||(pFila == 'd')) || (pFila == '4'))
				{
					if(getEstadoAsiento(3, pColumna)=='D')
					{
						setEstadoAsiento(3, pColumna, 'S');
						lvRetorno = true;
					}
				}
				else
					if (((pFila == 'E')||(pFila == 'e')) || (pFila == '5'))
					{
						if(getEstadoAsiento(4, pColumna)=='D')
						{
							setEstadoAsiento(4, pColumna, 'S');
							lvRetorno = true;
						}
					}
					else
						if (((pFila == 'F')||(pFila == 'f')) || (pFila == '6'))
						{
							if(getEstadoAsiento(5, pColumna)=='D')
							{
								setEstadoAsiento(5, pColumna, 'S');
								lvRetorno = true;
							}
						}
						return lvRetorno;
}

//Metodo que se encarga de imprimir la matriz de asientos
string Asientos::Imprimir()
{
	stringstream lvMensaje;
	for(char lvFila = 1; lvFila < caMAX_FILAS; lvFila++)
	{
		lvMensaje << ObtenerLetraFila(lvFila);
		for(int lvColumna = 0; lvColumna < caMAX_COLUMNAS; lvColumna++)
		{
			lvMensaje << "| " << aAsientos[lvFila][lvColumna].getEstado() << " |";
			if (lvColumna == caMAX_COLUMNAS-1)
				lvMensaje << "\n----------------------------------------------------\n";
		}
	}
	return lvMensaje.str();
}

//Metodos requeridos para la impresion de la matriz de asientos en la interfaz

//Metodo que se situa en la primera posicion de la matriz, por lo que inicializa los atributos de fila y columna actual en 0 y 
// el atributo impresion finalizada lo inicializa en false que tiene como sentido indicar que la impresion de la matriz no finalizo
void Asientos::InicializarImpresionAsientos()
{
	aFilaActualImpresion = 0;
	aColumnaActualImpresion = 0;
	aImpresionFinalizada = false;
}

//Metodo que se encarga de avanzar en la posicion actual de la matriz
bool Asientos::AvanzarImpresion()
{
	bool lvRetorno = false;
	if(aImpresionFinalizada==false)
	{
		if(aFilaActualImpresion < caMAX_FILAS)
		{
			if(aColumnaActualImpresion < caMAX_COLUMNAS-1)
				aColumnaActualImpresion++;
			else
			{
				aFilaActualImpresion++;
				aColumnaActualImpresion = 0;
			}
		}
		if(aFilaActualImpresion == caMAX_FILAS)
			aImpresionFinalizada = true;
		else
			lvRetorno = true;
	}
	return lvRetorno;
}

//Metodo que se encarga de indicar si la impresion finalizo
bool Asientos::ImpresionFinalizada()
{
	return aImpresionFinalizada;
}

//Metodo que se encarga de obtener la letra referente de la fila actual
char Asientos::getFilaActualImpresion()
{
	return ObtenerLetraFila(aFilaActualImpresion);
}

//Metodo encargado de retornar el estado segun la fila y columna actual
char Asientos::getEstadoAsientoActualImpresion()
{
	char lvRetorno = ' ';
	if(aImpresionFinalizada == false)
		lvRetorno = getEstadoAsiento(aFilaActualImpresion, aColumnaActualImpresion);
	return lvRetorno;
}
