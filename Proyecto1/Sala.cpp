#include "Sala.h"

//Metodo constructor sin parametros de la clase
Sala::Sala(void)
{
}

//Metodo constructor con parametros de la clase
Sala::Sala(string pTipo, string pNombre)
{
	aNombre = pNombre;
	aTipo = pTipo;
	aAsientos = Asientos();
}

//metodo que se encarga de modificar el valor del nombre de la sala
void Sala::setNombre(string pNombre)
{
	aNombre = pNombre;
}

//metodo que se encarga de retornar el nombre de la sala
string Sala::getNombre()
{
	return aNombre;
}

//metodo que se encarga de modificar el valor del tipo de sala (Regular o VIP)
void Sala::setTipo(string pTipo)
{
	aTipo = pTipo;
}

//Metodo que se encarga de retornar el valor del tipo de sala (Regular o VIP)
string Sala::getTipo()
{
	return aTipo;
}

//Metodo encargado de actualizar los asientos
void Sala::setAsientos(Asientos pAsientos)
{
	aAsientos = pAsientos;
}

//Metodo que se encarga de retornar los asientos
Asientos* Sala::getAsientos()
{
	return &aAsientos;
}

//Metodo destructor de la clase
Sala::~Sala(void)
{
}

//Metodo encargado de imprimir la informacion de la sala
string Sala::ImprimirSala()
{
	stringstream lvMensaje;
	lvMensaje << "Nombre de la sala: " << getNombre() << "\n";
	lvMensaje << "Tipo de sala: " << getTipo() << "\n";
	//Asientos* lvAsientos = NULL;
	return lvMensaje.str();
}
