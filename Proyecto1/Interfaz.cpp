#include "Interfaz.h"
#include <conio.h>
#include <string.h>

//Metodo constructor de la clase
Interfaz::Interfaz()
{
	aTituloGeneral = "Programa cinemas CR";
	aSubtitulo1 ="";
	aSubtitulo2 ="";
	for (int i = 0; i < caMAX_Horario; i++)
		aHorario[i] = " ";
	for (int i = 0; i < caMAX_Salas; i++)
		aSala[i];
	for (int i =0; i < caMAX_FuncionSemanal; i++)
		aFuncionSemanal[i] = new ListaFuncionDiaria();
	aListaPelicula = new ListaPelicula();

}

//Metodo destructor de la clase
Interfaz::~Interfaz(void)
{
}
//--------------------------------------------------------------------------------------
//Metodos que ingresan de forma estatica cierta informacion que se requiere al iniciar los procesos de la interfaz

//Metodo que se encarga de modificar el color de un texto
void SetColorX(int pColor)
{
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), pColor);
    return;
}

//Metodo que se encarga de retornar un texto con un color seleccionado
void Interfaz::Desplegar(string pString, int pColor)
{
	SetColorX(pColor);
	cout << pString;
}

//Metodo encargado de mostrar todos los titulos del sistema
void Interfaz::DesplegarEncabezado()
{
	cout << "MAYKOL BURGOS MESEN" << "\n";
	cout << "Universidad Nacional" << "\n";
	cout << "Programacion I" << "\n";
	cout << aTituloGeneral << "\n"; 
	cout << aSubtitulo1 << "\n";
	cout << aSubtitulo2 << "\n";
}

//Se le asignan valores arbritarios al vector de horario
void Interfaz::LlenarHorario()
{
	aHorario[0] = "11:00 am";
	aHorario[1] = "1:00 pm";
	aHorario[2] = "3:00 pm";
	aHorario[3] = "5:00 pm";
	aHorario[4] = "7:00 pm";

}

//Se le asignan valores arbritarios al vector de las salas
void Interfaz::LlenarSalas()
{
	aSala[0].setNombre("Sala 1");
	aSala[1].setNombre("Sala 2");
	aSala[0].setTipo("VIP");
	aSala[1].setTipo("Regular");
}

//Se asignan los dias de la semana a cada funcion diaria almacenada en el vector de funcion semanal
void Interfaz::LlenarNombresSamana()
{
	aFuncionSemanal[0] -> setNombre("Lunes");
	aFuncionSemanal[1] -> setNombre("Martes");
	aFuncionSemanal[2] -> setNombre("Miercoles");
	aFuncionSemanal[3] -> setNombre("Jueves");
	aFuncionSemanal[4] -> setNombre("Viernes");
	aFuncionSemanal[5] -> setNombre("Sabado");
	aFuncionSemanal[6] -> setNombre("Domingo");

}

//Metodo que restringe el ingreso de texto 
int Interfaz::LeerInt(string pTexto, string pMensajeError)
{
	int lvRetorno = 0;
	string lvStrTmp = "";
	bool lvIngresoCorrecto = false;
	do
	{
		try
		{
			cout << pTexto;
			cin >> lvStrTmp;
			lvRetorno = stoi(lvStrTmp);
			lvIngresoCorrecto = true;
		}
		catch(exception lvException)
		{
			cout << pMensajeError;

		}
	}while(lvIngresoCorrecto==false);
	return lvRetorno;
}

//Muestra un submenu para buscar peliculas por genero, etc
void Interfaz::IniciarBusquedaListaPelicula()
{
	char lvOpcion = ' ';
	int lvOpcionBuscar;
	cout << "Peliculas disponibles: " << "\n";
	aListaPelicula -> Imprimir();
	cout << "Desea buscar alguna pelicula en especifico?";
	cin >> lvOpcion;
	if ((lvOpcion == 's')||(lvOpcion == 'S'))
	{
		cout << "Elija como quiere buscar su peliucula:" << "\n";
		cout << "1) Busqueda por nombre" << "\n";
		cout << "2) Busqueda por genero" << "\n";
		cout << "3) Busqueda por idioma(Espannol/Subtitulada)" << "\n";
		cout << "4) Busqueda por tipo(3D/Regular)" << "\n";
		cout << "5) Busqueda por tipo de publico (Adultos/Ninnos)" << "\n";
		cout << "Digite la opcion que desea iniciar o 0 para salir: ";
		cin >> lvOpcionBuscar;
		if (lvOpcionBuscar != 0)
			BuscarListaPelicula(lvOpcionBuscar);
	}
}

//Encargado de manejar las opciones del metodo IniciarBusquedaListaPelicula 
void Interfaz::BuscarListaPelicula(int pOpcionBuscar)
{
	string lvNombrePelicula;
	string lvGenero;
	string lvTipoPelicula;
	string lvTipoPublico;
	string lvIdioma;
	if (pOpcionBuscar == 1)
	{
		cout << "Ingrese el nombre de la pelicula: ";
		cin >> lvNombrePelicula;
		cout << "Peliculas disponibles: " << "\n";
		cout << aListaPelicula -> BusquedaNombre(lvNombrePelicula);
	}
	else 
		if (pOpcionBuscar == 2)
		{
			cout << "Ingrese el genero de la pelicula: ";
			cin >> lvGenero;
			cout << "Peliculas disponibles: " << "\n";
			cout << aListaPelicula -> BusquedaGenero(lvGenero);
		}
		else
			if (pOpcionBuscar == 3)
			{
				cout << "Ingrese el Idioma de la pelicula: ";
				cin >> lvIdioma;
				cout << aListaPelicula -> BusquedaIdioma(lvIdioma);
			}
			else
				if (pOpcionBuscar == 4)
				{
					cout << "Ingrese el tipo de pelicula: ";
					cin >> lvTipoPelicula;
					cout << aListaPelicula -> BusquedaTipo(lvTipoPelicula);
				}
				else
					if (pOpcionBuscar == 5)
					{
						cout << "Ingrese el tipo de publico de la pelicula: ";
						cin >> lvTipoPublico;
						cout << aListaPelicula -> BusquedaTipoPublico(lvTipoPublico);
					}
}

//Metodo que se encarga de buscar la sala de una  funcion especifica
Sala* Interfaz::BuscarSalaFuncion(string pNombreFuncion, string pHorario, string pCodigoPelicula)
{
	Sala* lvSala = NULL;
	for (int i = 0; ((i<caMAX_FuncionSemanal)&&(lvSala==NULL)); i++)
		lvSala = aFuncionSemanal[i] -> BuscarSala(pNombreFuncion, pHorario, pCodigoPelicula);
	return lvSala;
}
void Interfaz::InicializarEstructuras()
{
	LlenarHorario();
	LlenarSalas();
	LlenarNombresSamana();
}
void Interfaz::ProcesarIngresoDatos()
{
	int lvOpcionIniciar = 0;
	cout << "1) Ingresar manualmente los datos" << "\n";
	cout << "2) Ingresar automaticamente los datos" << "\n";
	cin >> lvOpcionIniciar;
	if (lvOpcionIniciar == 1)
		IniciarUsuarioAdm();
	else
		if (lvOpcionIniciar == 2)
			IniciarDemo();
}
char Interfaz::ProcesarModoDeAcceso()
{
	char lvOpcion = ' ';
	char lvTipoUsuario = 0;
	cout << "Digite el tipo de usuario con el que desea ingresar al sistema " << "\n";
	cout << "1) Usuario Administrador" << "\n";
	cout << "2) Usuario Cliente" << "\n";
	lvTipoUsuario = _getch();
	//system("pause");
	system("cls");
	if (lvTipoUsuario == 49)
		ProcesarIngresoDatos();
	else
		if (lvTipoUsuario == 50)
			IniciarUsuarioCliente();
	cout << "Desea volver al menu inicial[S/N]?" << "\n";
	cin >> lvOpcion;
	system("pause");
	system ("cls");
	return lvOpcion;
}
//Metodo principal que se encarga de llamar a todos los metodo necesarios y generar toda la informacion de la interfaz
void Interfaz::Iniciar()
{
	char lvOpcion = ' ';
	InicializarEstructuras();
	do
	{
		lvOpcion = ProcesarModoDeAcceso();
	}
	while((lvOpcion == 's')||(lvOpcion == 'S'));
}
//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
//Metodos que se encargan de iniciar todos los procesos relacionados con el usuario administrador

//Metodo que inicia el usuario administrador
void Interfaz::IniciarUsuarioAdm()
{
	char lvOpcion = ' ';
	do
	{
		aSubtitulo1 = "Modo Administrador";
		DesplegarEncabezado();
		CrearListaPelicula();
		system("pause");
		system("cls");
		EliminarPelicula();
		system("pause");
		system("cls");
		if (aListaPelicula -> ListaVacia() == false)
		{
			LlenarFuncionSemanal();
			system("pause");
			system("cls");
			cout << "Funciones administrativas ingresadas correctamente" << "\n";
			ImprimirFuncionSemanal();
			cout << "�Desea volver al menu principal? [S/N]" << "\n";
		}
		else 
			cout << "�Desea volver al menu principal? [S/N]" << "\n";
	}
	while ((lvOpcion == 'N')||(lvOpcion == 'n'));
}

//Metodo encargado de manejar todo el proceso relacionado con la creacion e implementacion de la lista pelicula en la clase
void Interfaz::CrearListaPelicula()
{
	char lvOpcion = ' ';
	//Condicional que ingresa una pelicula cuando la lista se encuentra vacia
	if (aListaPelicula -> ListaVacia() == true)
	{
		cout << "Actualmente no se encuentran peliculas guardadas. �Desea ingresar una nueva? [S/N]" << "\n";
		cin >> lvOpcion;
		//solo si se digita S de si entra a crear la pelicula si digita N pasa al else
		if (lvOpcion == 'S' || lvOpcion == 's')
		{
			do
			{
				IngresarPelicula();
				cout << "Pelicula ingresada correctamente" << "\n";
				cout << "�Desea ingresar otra? [S/N]" << "\n";
				cin >> lvOpcion;
			}
			while ((lvOpcion == 'S')||(lvOpcion == 's'));
		}
		//Si no quiere ingresar peliculas
		cout << "Peliculas ingresadas: " << aListaPelicula -> Imprimir();
		if (aListaPelicula -> ListaVacia() == true)
			cout << "No hay peliculas" << "\n";
	}
	//Si la lista no esta vacia
	else
	{
		cout << "Actualmente se encuentran almacenadas n peliculas " << "\n";
		cout << "�Desea ingresar otra? [S/N]" << "\n";
		cin >> lvOpcion;
		//solo si se digita S de si entra a crear la pelicula si digita N pasa al else
		if (lvOpcion == 'S' || lvOpcion == 's')
		{
			do
			{
				IngresarPelicula();
				cout << "Pelicula ingresada correctamente" << "\n";
				cout << "�Desea ingresar otra? [S/N]" << "\n";
				cin >> lvOpcion;
			}
			while ((lvOpcion == 'S')||(lvOpcion == 's'));
		}
		//Si no quiere ingresar peliculas
		if (aListaPelicula -> ListaVacia() == false)
			cout << "Peliculas ingresadas: " << aListaPelicula -> Imprimir() << "\n";
		else
			cout << "No hay peliculas" << "\n";
	}
}

//Metodo que se encarga de controlar el ingreso de los datos necesarios para la integracion de una pelicula en la lista
void Interfaz::IngresarPelicula()
{
	string lvCodigo = "";
	string lvNombre = "";
	string lvGenero = "";
	string lvTipo = "";
	string lvTipoPublico = "";
	string lvIdioma = "";
	string lvSinopsis = "";
	cin.ignore(1024, '\n');
	cout << "Ingrese el codigo de la pelicula" << "\n";
	getline(cin, lvCodigo);
	cout << "Ingrese el nombre de la pelicula" << "\n";
	getline(cin, lvNombre);
	cout << "Ingrese el genero de la pelicula" << "\n";
	getline(cin, lvGenero);
	cout << "Ingrese el tipo de pelicula (3D/Regular)" << "\n";
	getline(cin, lvTipo);
	cout << "Ingrese el tipo de publico (Adultos/Ninnos)" << "\n";
	getline(cin, lvTipoPublico);
	cout << "Ingrese el idioma de pelicula (Espannol/Subtitulada)" << "\n";
	getline(cin, lvIdioma);
	cout << "Ingrese la sinopsis de la pelicula" << "\n";
	getline(cin, lvSinopsis);
	aListaPelicula -> AgregarInicio(lvCodigo, lvNombre, lvGenero, lvTipo, lvTipoPublico, lvIdioma, lvSinopsis);
}

//Metodo encargado de ingresar manualmente la informacion de cada funcion diaria
void Interfaz::LlenarFuncionSemanal()
{
	char lvOpcion = ' ';
	for (int i = 0; i < caMAX_FuncionSemanal; i++)
	{
		cout << "Desea ingresar una funcion para el dia " << aFuncionSemanal[i] -> getNombre() << ":";
		cin >> lvOpcion;
		if ((lvOpcion == 's')||(lvOpcion == 'S')) 
			IngresarFuncionesDiarias(aFuncionSemanal[i]);
	}

}

//Metodo encargado de ingresar la funcion diaria en el vector de funcion semanal
void Interfaz::IngresarFuncionesDiarias(ListaFuncionDiaria* pFuncionDiaria)
{
	char lvOpcion = ' ';
	string lvCodigoPelicula = "";
	string lvTipoSala = "";
	for (int i = 0; i < caMAX_Horario; i++)
	{
		for (int j = 0; j < caMAX_Salas; j++)
		{
			aSubtitulo2 = "Ingreso de funcion para el dia " + pFuncionDiaria -> getNombre();
			DesplegarEncabezado();
			cout << "Ingresar una funcion para la " + aSala[j].getNombre() + " horario " + aHorario[i] << "? :";
			cin >> lvOpcion;
			if ((lvOpcion == 's')||(lvOpcion == 'S'))
			{
				cout << "\n";
				cout << "Peliculas disponibles: " << aListaPelicula -> Imprimir() << "\n";
				cout << "Tome en cuenta que si el codigo ingresado o la sala ingresada no existe debera volver a ingresar dicho valor" << "\n";
				do
				{ 
					cout << "Ingrese el codigo de la pelicula para la " + aSala[j].getNombre() + " horario" + aHorario[i] + ":" << "\n";
					cin >> lvCodigoPelicula;
				}
				while (aListaPelicula -> ExistePelicula(lvCodigoPelicula) == false);
				pFuncionDiaria -> AgregarFinal(aHorario[i], aSala[j].getTipo(), aSala[j].getNombre(), aListaPelicula -> BuscarPelicula(lvCodigoPelicula));
				cout << "Funcion ingresada correctamente" << "\n";
			}
		}
	}
}

//Metodo que se encarga de eliminar una pelicula de la lista
void Interfaz::EliminarPelicula()
{
	char lvOpcion = ' ';
	string lvCodigo = "";
	cout << "Desea eliminar alguna pelicula?: ";
	cin >> lvOpcion;
		if ((lvOpcion == 's')||(lvOpcion == 'S'))
		{
			do
			{
				cout << "Peliculas ingresadas: " << aListaPelicula -> Imprimir();
				cout << "Ingrese el codigo de la pelicula que desea eliminar: ";
				cin >> lvCodigo;
				aListaPelicula -> Elimina(lvCodigo); 
				cout << "Pelicula eliminada" << "\n";
				if (aListaPelicula -> ListaVacia() == false)
				{
					cout << "Desea eliminar otra pelicula?: ";
					cin >> lvOpcion;
				}
				else
					cout << "No hay peliculas para eliminar" << "\n";
			}
			while ((lvOpcion == 's')||(lvOpcion == 'S'));
		}
}
//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
//Metodos que automaticamente cargan los datos de la lista pelicula, listaFuncionDiaria y vector funcion semanal

//Genera automatica todos los datos que deben ser ingresados por el usuario Adm
void Interfaz::IniciarDemo()
{
	LlenarListaPelicula();
	LlenarFuncionSemanalAutomatica();
}

//Metodo que Simplifica el metodo iniciar demo
void Interfaz::LlenarPelicula(string pCodigo, string pNombre, string pGenero, string pTipo, string pTipoPublico, string pIdioma, string pSinopsis, ListaPelicula* pListaPelicula)
{
	pListaPelicula -> AgregarInicio(pCodigo, pNombre, pGenero, pTipo, pTipoPublico, pIdioma, pSinopsis);
}

//Metodo encargado de ingresar automaticamente cada pelicula de la lista
void Interfaz::LlenarListaPelicula()
{
	//Lleando de peliculas
	//Pelicula 1
	LlenarPelicula("TEMB19", "7:19 La Hora del Temblor", "Drama", "Regular", "Adultos", "Subtitulada",	"Mart�n y Fernando se encuentran en la recepci�n del edificio donde trabajan. De pronto, un terremoto los sepulta debajo de siete pisos de concreto y metal retorcido.", aListaPelicula);
	//Pelicula 2
	LlenarPelicula("ATEN24", "Atentado en Paris", "Accion", "Regular", "Adultos", "Espannol", "Michael Mason (Richard Madden) es un carterista estadounidense que vive en Par�s que de repente se encuentra en manos de la CIA al robar un bolso que contiene algo m�s que una cartera.", aListaPelicula);
	//Pelicula 3
	LlenarPelicula("CIGU17", "Ciguennas", "Infantil", "3D", "Ninnos", "Espannol",	"Desesperados por entregar este paquete de puros problemas antes de que el jefe se entere, Junior y su amiga Tulip, la �nica humana en toda la Monta�a de Cig�e�as, deben apurarse a hacer su primera entrega de beb�s", aListaPelicula);
	//Pelicula 4
	LlenarPelicula("BEBE10", "El Bebe de Bridget Jones", "Comedia", "Regular", "Ninnos", "Espannol",	"Despu�s de quince a�os, la insegura Bridget Jones regresa a la gran pantalla con la maternidad como nuevo objetivo vital.", aListaPelicula);
	//Pelicula 5
	LlenarPelicula("ESPE01", "El Especialista", "Accion", "3D", "Adultos", "Subtitulada",	"Arthur Bishop (Jason Statham) pens� que hab�a dejado atr�s su pasado criminal, pero se encuentra de nuevo con �l en el momento en que la mujer de su vida es secuestrada por uno de sus mayores enemigos.", aListaPelicula);
	//Pelicula 6
	LlenarPelicula("ESCU12", "Escuadron Suicida", "Accion", "Regular", "Adultos", "Subtitulada",	"Se siente bien ser el malo� Formar un equipo integrado por los supervillanos encarcelados m�s peligrosos del mundoSe siente bien ser el malo� Formar un equipo integrado por los supervillanos encarcelados m�s peligrosos del mundo", aListaPelicula);
	//Pelicula 7
	LlenarPelicula("7MAGNI", "Los Siete Magnificos", "Accion", "3D", "Adultos", "Subtitulada",	"El director Antoine Fuqua nos ofrece esta moderna visi�n de una historia cl�sica en The Magnificent Seven, de Metro-Goldwyn-Mayer Pictures y Columbia Pictures.", aListaPelicula);
	//Pelicula 8
	LlenarPelicula("DRAG71", "Mi Amigo el Dragon", "Infantil", "3D", "Ninnos", "Espannol",	"MI AMIGO EL DRAG�N, una nueva versi�n de la querida pel�cula familiar de Disney, cuenta la aventura de un ni�o hu�rfano llamado Pete y su mejor amigo, Elliott, que da la casualidad es un drag�n.", aListaPelicula);
	//Pelicula 9
	LlenarPelicula("RESP00", "No Respires", "Terror", "Regular", "Adultos", "Subtitulada",	"Un grupo de amigos asaltan la casa de un hombre rico, y ciego, pensando que lograr�n el robo perfecto. Est�n equivocados.", aListaPelicula);
	//Pelicula 10
	LlenarPelicula("RESC98", "Rescate Suicida", "Accion", "3D", "Adultos", "Subtitulada",	"Un antiguo agente de la CIA es secuestrado por un grupo terrorista. Cuando su hijo se da cuenta de que no hay ning�n plan para sacar a su padre de ah�, organiza su propia operativa de rescate por su cuenta.", aListaPelicula);
}

//Metodo encargado de ingresar automaticamente cada funcion diaria del vector
void Interfaz::LlenarFuncionSemanalAutomatica()
{
	//Llenar funciones diarias
	//Lunes
	aFuncionSemanal[0] -> AgregarFinal(aHorario[0], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("TEMB19"));
	aFuncionSemanal[0] -> AgregarFinal(aHorario[0], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("DRAG71"));
	aFuncionSemanal[0] -> AgregarFinal(aHorario[1], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("7MAGNI"));
	aFuncionSemanal[0] -> AgregarFinal(aHorario[1], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("ESCU12"));
	aFuncionSemanal[0] -> AgregarFinal(aHorario[2], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("TEMB19"));
	aFuncionSemanal[0] -> AgregarFinal(aHorario[2], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("7MAGNI"));
	aFuncionSemanal[0] -> AgregarFinal(aHorario[3], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("ESCU12"));
	aFuncionSemanal[0] -> AgregarFinal(aHorario[3], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("BEBE10"));
	aFuncionSemanal[0] -> AgregarFinal(aHorario[4], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("ATEN24"));
	aFuncionSemanal[0] -> AgregarFinal(aHorario[4], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("CIGU17"));
	//Martes
	aFuncionSemanal[1] -> AgregarFinal(aHorario[0], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("BEBE10"));
	aFuncionSemanal[1] -> AgregarFinal(aHorario[0], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("ESPE01"));
	aFuncionSemanal[1] -> AgregarFinal(aHorario[1], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("ESCU12"));
	aFuncionSemanal[1] -> AgregarFinal(aHorario[1], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("ATEN24"));
	aFuncionSemanal[1] -> AgregarFinal(aHorario[2], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("TEMB19"));
	aFuncionSemanal[1] -> AgregarFinal(aHorario[2], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("RESC98"));
	aFuncionSemanal[1] -> AgregarFinal(aHorario[3], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("ESCU12"));
	aFuncionSemanal[1] -> AgregarFinal(aHorario[3], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("CIGU17"));
	aFuncionSemanal[1] -> AgregarFinal(aHorario[4], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("ATEN24"));
	aFuncionSemanal[1] -> AgregarFinal(aHorario[4], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("CIGU17"));
	//Miercoles
	aFuncionSemanal[2] -> AgregarFinal(aHorario[0], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("DRAG71"));
	aFuncionSemanal[2] -> AgregarFinal(aHorario[0], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("CIGU17"));
	aFuncionSemanal[2] -> AgregarFinal(aHorario[1], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("ESCU12"));
	aFuncionSemanal[2] -> AgregarFinal(aHorario[1], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("ESPE01"));
	aFuncionSemanal[2] -> AgregarFinal(aHorario[2], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("RESP00"));
	aFuncionSemanal[2] -> AgregarFinal(aHorario[2], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("7MAGNI"));
	aFuncionSemanal[2] -> AgregarFinal(aHorario[3], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("ESCU12"));
	aFuncionSemanal[2] -> AgregarFinal(aHorario[3], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("TEMB19"));
	aFuncionSemanal[2] -> AgregarFinal(aHorario[4], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("ESCU12"));
	aFuncionSemanal[2] -> AgregarFinal(aHorario[4], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("RESC98"));
	//Jueves
	aFuncionSemanal[3] -> AgregarFinal(aHorario[0], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("TEMB19"));
	aFuncionSemanal[3] -> AgregarFinal(aHorario[0], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("DRAG71"));
	aFuncionSemanal[3] -> AgregarFinal(aHorario[1], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("7MAGNI"));
	aFuncionSemanal[3] -> AgregarFinal(aHorario[1], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("ESCU12"));
	aFuncionSemanal[3] -> AgregarFinal(aHorario[2], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("TEMB19"));
	aFuncionSemanal[3] -> AgregarFinal(aHorario[2], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("7MAGNI"));
	aFuncionSemanal[3] -> AgregarFinal(aHorario[3], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("ESCU12"));
	aFuncionSemanal[3] -> AgregarFinal(aHorario[3], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("BEBE10"));
	aFuncionSemanal[3] -> AgregarFinal(aHorario[4], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("ATEN24"));
	aFuncionSemanal[3] -> AgregarFinal(aHorario[4], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("CIGU17"));
	//Viernes
	aFuncionSemanal[4] -> AgregarFinal(aHorario[0], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("BEBE10"));
	aFuncionSemanal[4] -> AgregarFinal(aHorario[0], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("ESPE01"));
	aFuncionSemanal[4] -> AgregarFinal(aHorario[1], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("ESCU12"));
	aFuncionSemanal[4] -> AgregarFinal(aHorario[1], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("ATEN24"));
	aFuncionSemanal[4] -> AgregarFinal(aHorario[2], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("TEMB19"));
	aFuncionSemanal[4] -> AgregarFinal(aHorario[2], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("RESC98"));
	aFuncionSemanal[4] -> AgregarFinal(aHorario[3], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("ESCU12"));
	aFuncionSemanal[4] -> AgregarFinal(aHorario[3], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("CIGU17"));
	aFuncionSemanal[4] -> AgregarFinal(aHorario[4], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("ATEN24"));
	aFuncionSemanal[4] -> AgregarFinal(aHorario[4], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("CIGU17"));
	//Sabado
	aFuncionSemanal[5] -> AgregarFinal(aHorario[0], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("DRAG71"));
	aFuncionSemanal[5] -> AgregarFinal(aHorario[0], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("CIGU17"));
	aFuncionSemanal[5] -> AgregarFinal(aHorario[1], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("ESCU12"));
	aFuncionSemanal[5] -> AgregarFinal(aHorario[1], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("ESPE01"));
	aFuncionSemanal[5] -> AgregarFinal(aHorario[2], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("RESP00"));
	aFuncionSemanal[5] -> AgregarFinal(aHorario[2], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("7MAGNI"));
	aFuncionSemanal[5] -> AgregarFinal(aHorario[3], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("ESCU12"));
	aFuncionSemanal[5] -> AgregarFinal(aHorario[3], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("TEMB19"));
	aFuncionSemanal[5] -> AgregarFinal(aHorario[4], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("ESCU12"));
	aFuncionSemanal[5] -> AgregarFinal(aHorario[4], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("RESC98"));
	//Domingo
	aFuncionSemanal[6] -> AgregarFinal(aHorario[0], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("TEMB19"));
	aFuncionSemanal[6] -> AgregarFinal(aHorario[0], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("DRAG71"));
	aFuncionSemanal[6] -> AgregarFinal(aHorario[1], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("7MAGNI"));
	aFuncionSemanal[6] -> AgregarFinal(aHorario[1], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("ESCU12"));
	aFuncionSemanal[6] -> AgregarFinal(aHorario[2], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("TEMB19"));
	aFuncionSemanal[6] -> AgregarFinal(aHorario[2], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("7MAGNI"));
	aFuncionSemanal[6] -> AgregarFinal(aHorario[3], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("ESCU12"));
	aFuncionSemanal[6] -> AgregarFinal(aHorario[3], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("BEBE10"));
	aFuncionSemanal[6] -> AgregarFinal(aHorario[4], aSala[0].getTipo(), aSala[0].getNombre(), aListaPelicula -> BuscarPelicula("ATEN24"));
	aFuncionSemanal[6] -> AgregarFinal(aHorario[4], aSala[1].getTipo(), aSala[1].getNombre(), aListaPelicula -> BuscarPelicula("CIGU17"));
}
//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
//Metodos encargados de iniciar todos los procesos del usuario cliente
void Interfaz::IniciarUsuarioCliente()
{
	char lvOpcion = ' ';
	do
	{
		aSubtitulo1 = "Modo Cliente";
		DesplegarEncabezado();
		if (aListaPelicula -> ListaVacia() == true)
			cout << "Lo sentimos, actualmente el sistema no tiene ninguna funcion para mostrar";
		else
			LlenarOrdenCompra();
	}
	while ((lvOpcion == 'S')||(lvOpcion == 's'));
}

//Metodo encargado de ingresar toda la informacion que el cliente digita, ademas las guarda en la orden de compra
void Interfaz::LlenarOrdenCompra()
{
	char lvOpcionSalida = ' ';
	do
	{
		cin.ignore(1024, '\n');
		ImprimirFuncionSemanal();
		system("pause");
		system("cls");
		IniciarBusquedaListaPelicula();
		system("pause");
		system("cls");
		IngresoPeliculaCliente();
		system("pause");
		system("cls");
		IngresoDiaCliente(aOrdenCompra.getCodigoPelicula());
		system("pause");
		system("cls");
		IngresoCantidadTickets(aOrdenCompra.getCodigoPelicula());
		system("pause");
		system("cls");
		ManejoDeAsientos(aOrdenCompra.getCantidadAdultos(), aOrdenCompra.getCantidadNinnos(), aOrdenCompra.getDiaFuncion(), aOrdenCompra.getHorarioFuncion(), aOrdenCompra.getCodigoPelicula());
		system("pause");
		system("cls");
		ImprimirOrdenCompra();
		system("pause");
		system("cls");
		cout << "Gracias por la compra" << "\n";
		cout << "Desea volver a comprar una funcion[S/N]?";
		cin >> lvOpcionSalida;
	}
	while ((lvOpcionSalida == 'S')||(lvOpcionSalida == 's'));
}

//Metodo que se encarga de gestionar todos los procesos de interaccion con el cliente respecto a la pelicula
void Interfaz::IngresoPeliculaCliente()
{
	cin.ignore(1024, '\n');
	string lvCodigoPelicula = "";
	cout << "Peliculas disponibles: " << "\n";
	cout << aListaPelicula -> Imprimir() << "\n";
	cout << "Tome en cuenta que si digita mal el codigo debera volver a ingresarlo" << "\n";
	do
	{
		cout << "Ingrese el codigo de la pelicula que desea ver: " << "\n";
		getline(cin, lvCodigoPelicula, '\n');
	}
	while (aListaPelicula -> ExistePelicula(lvCodigoPelicula) == false);
	aOrdenCompra.setCodigoPelicula(lvCodigoPelicula);
}

//Metodo que se encarga de gestionar todos los procesos de interaccion con el cliente respecto al dia de la funcion
void Interfaz::IngresoDiaCliente(string pCodigoPelicula)
{
	string lvDiaFuncion = "";
	string lvHorario = "";
	string lvSala = "";
	string lvTipoSala = "";
	Sala lvTipoSalaFuncion;
	//--------------------------
	cout << "Ingrese el dia que desea ver la funcion: ";
	getline(cin, lvDiaFuncion, '\n');
	aOrdenCompra.setDiaFuncion(lvDiaFuncion);
	ImprimirFuncionXDia(lvDiaFuncion, pCodigoPelicula);
	cout << "Segun el cuadro anterior, digite el horario de la funcion que desea ver: ";
	getline(cin, lvHorario, '\n');
	aOrdenCompra.setHorarioFuncion(lvHorario);
	cout << "Segun el cuadro anterior, digite el nombre de la sala de la funcion que desea ver: ";
	getline(cin, lvSala,'\n');
	aOrdenCompra.setSalaFuncion(lvSala);
	cout << "Segun el cuadro anterior, digite el (tipo) de la sala de la funcion que desea ver: ";
	getline(cin, lvTipoSala,'\n');
	aOrdenCompra.setTipoSalaFuncion(lvTipoSala);
}

//Metodo que se encarga de gestionar todos los procesos de interaccion con el cliente respecto a la cantidad de tickets que va a comprar
//Si es una pelicula con tipo de publico Adultos no muestra la opcion de ingresar tickets para ninnos
void Interfaz::IngresoCantidadTickets(string pCodigoPelicula)
{
	int lvCantidadAdultos = 0;
	int lvCantidadNinnos = 0;
	Pelicula* lvPelicula;
	//----------------------------------------
	cout << "Cuantos ticketes desea comprar?";
	lvPelicula = aListaPelicula -> BuscarPelicula(pCodigoPelicula);
	if (lvPelicula -> getTipoPublico() == "Adultos" )
	{
		lvCantidadAdultos = LeerInt("\nAdultos: ", "ERROR, el valor debe ser numerico. Intente de nuevo...");
		aOrdenCompra.setCantidadAdultos(lvCantidadAdultos);
	}
	else
	{
		lvCantidadAdultos = LeerInt("\nAdultos: ", "ERROR, el valor debe ser numerico. Intente de nuevo...");
		aOrdenCompra.setCantidadAdultos(lvCantidadAdultos);
		lvCantidadNinnos = LeerInt("\nNinnos: ", "ERROR, el valor debe ser num�rico. Intente de nuevo...");
		aOrdenCompra.setCantidadNinnos(lvCantidadNinnos);
	}
}

//Metodo que se encarga de gestionar todos los procesos de interaccion con el cliente respecto a la escogencia de asientos
void Interfaz::ManejoDeAsientos(int pCantidadAdultos, int pCantidadNinnos, string pDiaFuncion, string pHorario, string pCodigoPelicula)
{
	char lvFilaAsiento = ' ';
	char lvColumnaAsiento = ' ';
	Sala* lvSalaFuncion;
	int lvSumaCantidadTickets = 0;
	Asientos* lvOpcionAsientos = NULL;
	Asientos lvOpcionAsientosRespaldo;
	char lvOpcion = ' ';
	//-----------------------------
	lvSumaCantidadTickets = pCantidadAdultos + pCantidadNinnos;
	lvSalaFuncion = BuscarSalaFuncion(pDiaFuncion, pHorario, pCodigoPelicula);
	//Saca un respaldo temporal del estado de todos los asientos antes de inciar el proceso de seleccion
	//con la intencion de restablecerlos en caso de que el usuario decida que los ya ingresados no estan bien
	//basicamente toma los valores apuntados y los copia en la variable local lvOpcionAsientosRespaldo
	lvOpcionAsientosRespaldo = *(lvSalaFuncion->getAsientos());
	for (int i = 0; i < lvSumaCantidadTickets; i++)
	{
		system("cls");
		cout << "Solicitando ubicacion de tickete " << i+1 << "\n";
		ImprimirAsientos(lvSalaFuncion);
		cout << "\n";
		cout << "Ingrese los asientos que desea" << "\n";
		cout << "Fila: ";
		cin >> lvFilaAsiento;
		cout << "Columna: ";
		cin >> lvColumnaAsiento;
		lvOpcionAsientos = lvSalaFuncion->getAsientos();
		if (lvOpcionAsientos->SeleccionarAsiento(lvFilaAsiento, lvColumnaAsiento-49)==false)
		{
			cout << "ERROR, el asiento ya esta ocupado. Proceda nuevamente..." << "\nPresione una tecla para continuar...";
			_getch();
			i--;
		}
	}
	system("pause");
	system("cls");
	lvOpcionAsientos = lvSalaFuncion->getAsientos();
	ImprimirAsientos(lvSalaFuncion);
	cout << "\n";
	cout << "La seleccion de los asientos es correcta[S/N]?";
	cin >> lvOpcion;
	system("pause");
	system("cls");
	if ((lvOpcion == 's')||(lvOpcion == 'S'))
		lvOpcionAsientos -> OcuparAsiento();
	else
	{
		//Restablece el valor de los asientos originales tomando el respaldo contenido en la variable local
		//e indicandole que los copie en la posicion de memoria a la que apunta lvOpcionAsientos. Dejandolos de esta manera
		//en su estado original
		*(lvOpcionAsientos) = lvOpcionAsientosRespaldo;
		ManejoDeAsientos(aOrdenCompra.getCantidadAdultos(), aOrdenCompra.getCantidadNinnos(), aOrdenCompra.getDiaFuncion(), aOrdenCompra.getHorarioFuncion(), aOrdenCompra.getCodigoPelicula());
	}
	ImprimirAsientos(lvSalaFuncion);
	cout << "\n";
}
//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
//Metodos de impresion

//Impresion de la funcion de un dia determinado y segun el codigo de la pelicula
void Interfaz::ImprimirFuncionXDia(string pDiaFuncion, string pCodigoPelicula)
{
	for (int i = 0; i < caMAX_FuncionSemanal; i++)
	{
		if (aFuncionSemanal[i] -> getNombre() == pDiaFuncion)
			cout << aFuncionSemanal[i] ->ImprimirXCodigo(pCodigoPelicula);
	}
}

//Impresion de la matriz de asientos en una sala determinada posicion por posicion
void Interfaz::ImprimirAsientos(Sala* pSala)
{
	pSala->ImprimirSala();
	Asientos* lvAsientos = pSala->getAsientos();
	lvAsientos -> InicializarImpresionAsientos(); 
	char lvFilaActual = lvAsientos->getFilaActualImpresion();
	SetColorX(02);
	cout << lvFilaActual << "|";
	while (lvAsientos->ImpresionFinalizada() == false)
	{
		if (lvFilaActual != lvAsientos->getFilaActualImpresion())
		{
			lvFilaActual = lvAsientos->getFilaActualImpresion();
			cout << "\n-----------------------------------------\n";
			cout << lvFilaActual << "|";
		}
		ImprimirAsiento(lvAsientos->getEstadoAsientoActualImpresion());
		lvAsientos->AvanzarImpresion();
	}
}

//Impresion del estado de un asiento determinado con su respectivo color Verde(D), Gris(S) o Rojo(O)
void Interfaz::ImprimirAsiento(char pEstado)
{
	if (pEstado == 'D')
		Desplegar("|D| ", 02);
	if (pEstado == 'S')
		Desplegar("|S| ", 07);
	if (pEstado == 'O')
		Desplegar("|O| ", 04);
}

//imprecion del vector de funcion semanal
void Interfaz::ImprimirFuncionSemanal()
{
	cout << "Funciones disponibles: " << "\n";
	for (int i = 0; i < caMAX_FuncionSemanal; i++)
		cout << aFuncionSemanal[i] -> ImpresionCompleta();
}

//Metodo encargado de imprimir toda la informacion que el cliente digita
void Interfaz::ImprimirOrdenCompra()
{
	cout << "Resumen de compra:" << "\n" << aOrdenCompra.Imprimir();
	cout << "\n";
}
//--------------------------------------------------------------------------------------