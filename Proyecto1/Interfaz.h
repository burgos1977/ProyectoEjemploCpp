#ifndef Interfaz_H
#define Interfaz_H

#pragma once
#include "Asientos.h"
#include "ListaFuncionDiaria.h"
#include "Sala.h"
#include "ListaPelicula.h"
#include "OrdenCompra.h"
#include <sstream>
#include <string>
#include <windows.h>
#include <iostream>


using namespace std;
const char caMAX_Horario = 5;
const char caMAX_Salas = 2;
const char caMAX_FuncionSemanal = 7;
class Interfaz
{
private:
	string aTituloGeneral;
	string aSubtitulo1;
	string aSubtitulo2;
	ListaFuncionDiaria* aFuncionSemanal[caMAX_FuncionSemanal];
	string aHorario[caMAX_Horario];
	Sala aSala[caMAX_Salas];
	ListaPelicula* aListaPelicula;
	OrdenCompra aOrdenCompra;

public:
	Interfaz();
	~Interfaz(void);
	void DesplegarEncabezado();
	void LlenarNombresSamana();
	void LlenarHorario();
	void LlenarSalas();
	void LlenarFuncionSemanal();
	void IngresarFuncionesDiarias(ListaFuncionDiaria*);
	void AgregarPelicula(string, string, string, string, string, string, string, ListaPelicula*);
	void InicializarEstructuras();
	void ProcesarIngresoDatos();
	char ProcesarModoDeAcceso();
	void Iniciar();
	void IniciarUsuarioAdm();
	void CrearListaPelicula();
	void IngresarPelicula();
	void EliminarPelicula();
	void CrearListaFuncionesDiarias();
	void IniciarDemo();
	void LlenarPelicula(string, string, string, string, string, string, string, ListaPelicula*);
	void LlenarListaPelicula();
	void LlenarFuncionSemanalAutomatica();
	void IniciarUsuarioCliente();
	void ImprimirFuncionSemanal();
	void LlenarOrdenCompra();
	void ImprimirFuncionXDia(string, string);
	void ImprimirAsientos(Sala*);
	void ImprimirAsiento(char);
	Sala* BuscarSalaFuncion(string, string, string);
	void Desplegar(string, int);
	int LeerInt(string, string);
	void IngresoPeliculaCliente();
	void IngresoDiaCliente(string);
	void IngresoCantidadTickets(string);
	void ManejoDeAsientos(int, int, string, string, string);
	void IniciarBusquedaListaPelicula();
	void BuscarListaPelicula(int);
	void ImprimirOrdenCompra();
};
#endif
