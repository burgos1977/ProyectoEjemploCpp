#include "NodoFuncionDiaria.h"

//Constructor con parametros de la clase
NodoFuncionDiaria::NodoFuncionDiaria(string pHorario, string pTipo, string pNombre, Pelicula* pPelicula)
{
	//Este puntero de la pelicula resive como parametro una direccion de memoria retornado en el metodo BuscarPelicula en la clase 
	//ListaPelicula con el fin de obtener directamente la informacion de la pelicula
	aPelicula = pPelicula;
	//Respecto a la sala y el horario, no es requerido que sean punteros ya que no es necesario manejar mucha informacion ni contiene
	//conplejos que puedan requerir de un puntero
	aHorario = Horario(pHorario);
	aSala = Sala(pTipo, pNombre);
	aSiguiente = NULL;
}

//Metodo que se encarga de modificar el valor de la pelicula
void NodoFuncionDiaria::setPelicula(Pelicula* pPelicula)
{
	aPelicula = pPelicula;
}

//Metodo que se encarga de retornar el valor de la pelicula
Pelicula* NodoFuncionDiaria::getPelicula()
{
	return aPelicula;
}

//Metodo que se encarga de modificar el valor del horario
void NodoFuncionDiaria::setHorario(Horario pHorario)
{
	aHorario = pHorario;
}

//Metodo que se encarga de modificar el valor del horario
Horario NodoFuncionDiaria::getHorario()
{
	return aHorario;
}

//Metodo que se encarga de modificar el valor dela sala
void NodoFuncionDiaria::setSala(Sala pSala)
{
	aSala = pSala;
}

//Metodo que se encarga de retonar el valor de la sala
Sala* NodoFuncionDiaria::getSala()
{
	return &aSala;
}

//Metodo que se encarga de modificar el valor del siguiente
void NodoFuncionDiaria::setSiguiente(NodoFuncionDiaria* pSiguiente)
{
	aSiguiente = pSiguiente;
}

//Metodo que se encarga de retornar el valor del siguiente
NodoFuncionDiaria* NodoFuncionDiaria::getSiguiente()
{
	return aSiguiente;
}

//Destructor de la clase
NodoFuncionDiaria::~NodoFuncionDiaria(void)
{
}

